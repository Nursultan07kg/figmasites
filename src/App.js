import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <>
      <div className='dv'>
        <div className='bs'>
          EXPOFORM
          <div className='ic'></div>
          <button className='tx'>о комплексе</button>
          <button className='tx'>о комплексе</button>
          <button className='tx'>о комплексе</button>
          <button className='tx'>о комплексе</button>
          <button className='tx'>о комплексе</button>
          <div className='ic_2'></div>
          <div className='ic_3 ic_2'></div>
          <p className='tx_2 tx'>English</p>
        </div>
        <p className='tx_3 tx'>ЭКСПОФОРУМ</p>
        <p className='tx_4 tx'>
          конгрессно-выставочный центр
          Санкт-Петербурга
        </p>
        <p className='tx_5 tx'>
          О комплексе
        </p>
        <div className='dvbos'>
          <div className='dv_1'>
            <p className='p_1'>Календарь
              мероприятий
            </p>
            <p className='px_1 tx_4'> Подробнее
            </p>
            <div className='icn_1'></div>
          </div>
          <div className='dv_1 dv_2'>
            <p className='p_1 p_2'>Как добраться до Экспофорума
            </p>
            <p className='px_1 tx_4'> Подробнее
            </p>
            <div className='icn_1'></div>
          </div>
          <div className='dv_1 dv_3'>
            <p className='p_1 p_3'>Онлайн-заявка на организацию мероприятия
            </p>
            <p className='px_1 tx_4'> Подробнее
            </p>
            <div className='icn_1'></div>
          </div>
          <div className='dv_1 dv_4'>
            <p className='p_1 p_4'>План и инфраструктура комплекса
            </p>
            <p className='px_1 tx_4'> Подробнее
            </p>
            <div className='icn_1'></div>
          </div>
        </div>
        <div className='bd_1'>
          <div className='bop'>
            <h5 className='tx_or'>Организаторам</h5>
            <h5 className='tx_or'>Экспонентам</h5>
            <h5 className='tx_or'>Посетителям</h5>
            <h5 className='tx_or'>Прессе</h5>
          </div>
          <a className='slk' href='https://www.figma.com/file/PBhOdtGj7HdyARf0mUvmPf/Untitled?type=design&node-id=1-9&t=AW5xyTc6JU5oiEl2-0'>Документы организаторам</a>
          <div className='bnp'>
            <p className='tx_mn'>
              Услуги организаторам Выставочные площади Рестораны и кафе Конгресс-центр Спорт-центр
            </p>
            <p className='tx_mn tx_mn_2'>
              Общие условия участия Услуги экспонентам Документы экспонентам Центр деловых контактов Забронировать гостиницу
            </p>
            <p className='tx_mn tx_mn_3'>
              Сервисы Онлайн-регистрация на мероприятие Забронировать гостиницу Центр деловых контактов
            </p>
            <p className='tx_mn tx_mn_4'>
              Аккредитация Пресс-служба Правила аккредитации
            </p>
          </div>
        </div>
        <div className='bd_ml'>
          <p className='a_tx'>Текущие и будущие мероприятия</p>
          <p className='b_tx'>Все мероприятие</p>
          <div className='sz'></div>
          <div className='io'></div>
          <div className='tl_1'>
            <div className='krt_1'>
              <p className='m_xt'>2 января 2020 г. – 7 января 2020 г.</p>
              <p className='x_xt m_xt'>ЭКСПО ЕЛКА</p>
              <p className='n_xt m_xt'>Парк интерактивных развлечений</p>
            </div>
            <div className='krt_1 krt_2'>
              <p className='m_xt'>5 февраля 2020 г. – 9 февраля 2020 г.</p>
              <p className='x_xt m_xt'>Junwex Петербург</p>
              <p className='n_xt m_xt'>Выставка ювелирных изделий</p>
            </div>
            <div className='krt_1 krt_3'>
              <p className='m_xt'>7 февраля 2020 г. – 9 февраля 2020 г.</p>
              <p className='x_xt m_xt'>Невский ларец</p>
              <p className='n_xt m_xt'>Выставка-ярмарка народных художественных промыслов и ремесел</p>
            </div>
            <div className='krt_1 krt_4'>
              <p className='m_xt'>14 февраля 2020 г. – 23 февраля 2020 г.</p>
              <p className='x_xt m_xt'>ПОНАЕХАЛИ!</p>
              <p className='n_xt m_xt'>Специализированная арт-ярмарка</p>
            </div>
            <div className='krt_1 krt_5'>
              <p className='m_xt'>18 февраля 2020 г. – 21 февраля 2020 г.</p>
              <p className='x_xt m_xt'>VET.CAMP</p>
              <p className='n_xt m_xt'>Конференция для ветеринарных врачей</p>
            </div>
            <div className='krt_1 krt_6'>
              <p className='m_xt'>26 февраля 2020 г. – 28 февраля 2020 г.</p>
              <p className='x_xt m_xt'>ExpoHoReCa</p>
              <p className='n_xt m_xt'>Специализированная выставка</p>
            </div>
          </div>
          <div className='bd_p'>
            <div className='bfbs'>
              <div className='bsb'>
                <div className='kri'></div>
                <div className='kri kri_2'></div>
              </div>
              <p className='k_tx'>Новости</p>
              <u className='d_tx k_tx'>Все новости______________</u>
              <div className='hrt k_tx'></div>
              <div className='gsgs'>
                <div className='y_pp'>
                  <p className='h_tx'>4 января 2020 г.</p>
                  <p className='h_tx_2 h_tx'>Как объединилась реальность и виртуальность – LIKEE PARTY</p>
                </div>
                <div className='y_pp y_pp_2'>
                  <p className='h_tx'>2 января 2020 г.</p>
                  <p className='h_tx_2 h_tx'>Как объединилась реальность и виртуальность – LIKEE PARTY</p>
                </div>
                <div className='y_pp y_pp_3'>
                  <p className='h_tx'>2 января 2020 г.</p>
                  <p className='h_tx_2 h_tx'>Как объединилась реальность и виртуальность – LIKEE PARTY</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='f_bd'>
          <div className='cl_bs'>
            <div className='bsk'>
              <p className='txx_1'>О компании
              </p>
              <a className='slk slk_2' href='https://www.figma.com/file/PBhOdtGj7HdyARf0mUvmPf/Untitled?type=design&node-id=1-9&t=AW5xyTc6JU5oiEl2-0'>О нашей компании</a>
              <div className='txx_2'>Вакансии Партнеры Контакты</div>
            </div>
            <div className='bsk'>
              <p className='txx_1'>Экспонентам
              </p>
              <div className='txx_2 txx_3'>Общие условия участия Услуги экспонентам Документы экспонентам</div>
            </div>
            <div className='bsk'>
              <p className='txx_1'>Прессе
              </p>
              <div className='txx_2 txx_3'>Аккредитация Пресс-служба Правила аккредитации</div>
            </div>
            <div className='bsk'>
              <p className='txx_1'>Организаторам
              </p>
              <div className='txx_2 txx_3'>Преимущества площадки Структура комплекса Онлайн-заявка на организацию Документы организаторам</div>
            </div>
            <div className='bsk'>
              <p className='txx_1'>Посетителям
              </p>
              <div className='txx_2 txx_3'>Сервисы Онлайн-регистрация Центр деловых контактов Забронировать гостиницу</div>
            </div>
            <div className='bsk'>
              <p className='txx_1'>Приложение
              </p>
              <div className='txx_2'>
                <div className='apple_1'>
                  <div className='apple_3 apple_2'></div>
                  <div className='apple_2'></div>
                  <p className='trx'>Загрузите в</p>
                  <p className='trx trx_2'>App Store</p>
                </div>
              </div>
              <div className='txx_2'>
                <div className='apple_1'>
                  <div className='apple_3 pley_1'></div>
                  <p className='trx tr_1'>Доступно в</p>
                  <p className='trx trx_2 tr_2'>Google play</p>
                </div>
              </div>
            </div>
          </div>
          <div className='cl_gs'>
            <p className='tp_tx'>Мы в соц. сетях</p>
            <div className='vk'></div>
            <div className='vk vk_2'></div>
            <div className='vk vk_3'></div>
            <div className='vk vk_4'>
              <div className='vk vk_5'></div>
              <p className='iop'>Подписка на новости</p>
              <h4 className='poi'>Получайте все самые последние новости о мероприятиях в Экспофоруме.</h4>
              <div className='trt'>Введите ваш e-mail</div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default App;